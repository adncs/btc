﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTC.DTO
{
    public class Statistics
    {
        public Coin Coin { get; set; }

        public double OBV { get; set; }

        public double RSI { get; set; }

        public double StochRSI_MACD { get; set; }

        public double StochRSI_SIGNAL { get; set; }

        public double MACD { get; set; }

        public double SIGNAL { get; set; }
    }
}

﻿namespace BTC.DTO
{
    public enum Table
    {
        Quotation = 0,
        ClosingValues = 1,
        Orders = 2,
        Users = 3,
        Log = 4,
    }

    public static class Tables
    {
        public static string ToFriendlyString(this Table me)
        {
            switch (me)
            {
                case Table.ClosingValues:
                    return "MBT_FECHAMENTO";
                case Table.Quotation:
                    return "MERCADOBITCOIN";
                case Table.Orders:
                    return "RBT_TRAD";
                case Table.Users:
                    return "USR_CFIG";
                case Table.Log:
                    return "RBT_LOG";
                default:
                    return "";
            }
        }
    }
}

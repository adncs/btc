﻿using System;

namespace BTC.DTO
{
    public enum Operation
    {
        None = 0,
        Buy = 1,
        Sell = 2,
    };

    public enum Sense
    {
        Neuter = 0,
        Falls = 1,
        High = 2,
    };

    public static class Operations
    {
        public static string ToFriendlyString(this Operation me)
        {
            switch (me)
            {
                case Operation.None:
                    return "Nenhuma";
                case Operation.Buy:
                    return "Compra";
                case Operation.Sell:
                    return "Venda";
                default:
                    return string.Empty;
            }
        }
    }

    public class Orders
    {
        public int Id { get; set; }

        public Exchange Exchange { get; set; }

        public string Currency { get; set; }

        public double Value { get; set; }

        public Operation Sense { get; set; }

        public bool Released { get; set; }

        public DateTime Date { get; set; }
    }
}

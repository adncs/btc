﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTC.DTO
{
    public enum Coin
    {
        Bitcoin = 1,
        BitcoinCash = 2,
        Litecoin = 3
    }

    public static class Currencys
    {
        public static string ToFriendlyString(this Coin me)
        {
            switch (me)
            {
                case Coin.Bitcoin:
                    return "BTC";
                case Coin.BitcoinCash:
                    return "BCH";
                case Coin.Litecoin:
                    return "LTC";
                default:
                    return "";
            }
        }

        public static string ToFullString(this Coin me)
        {
            switch (me)
            {
                case Coin.Bitcoin:
                    return "BITCOIN";
                case Coin.BitcoinCash:
                    return "BITCOIN CASH";
                case Coin.Litecoin:
                    return "LITECOIN";
                default:
                    return "";
            }
        }
    }
}

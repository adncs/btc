﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTC.UTIL
{
    public class Constants
    {
        public const string FATAL_ERROR_OCCURRED = "Fatal error occurred in {0}";

        public const string DETAILS = "\tDetails: {0}";
    }
}

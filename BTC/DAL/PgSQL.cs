﻿using BTC.DTO;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace BTC.DAL
{
    public static class PgSQL
    {
        private static NpgsqlConnection connection;
        private static string server;
        private static string database;
        private static string uid;
        private static string password;

        //Initialize values
        private static void Initialize()
        {
            server = "v.brasilturbo.com";
            database = "bitcoin";
            uid = "postgres";
            password = "gta010203";
            string connectionString;
            connectionString = "SERVER=" + server + ";Port=1316;" + "DATABASE=" +
            database + ";" + "User Id=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new NpgsqlConnection(connectionString);
        }

        //open connection to database
        private static bool OpenConnection()
        {
            try
            {
                if (connection == null)
                    Initialize();

                connection.Open();
                return true;
            }
            catch (NpgsqlException ex)
            {
                ReportError(ex);
                return false;
            }
        }

        //Close connection
        private static bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (NpgsqlException ex)
            {
                ReportError(ex);
                return false;
            }
        }

        //Insert statement

        public static DataTable Select(Table tTable, string where)
        {
            DataTable data = new DataTable();

            string query = string.Empty;

            switch (tTable)
            {
                case Table.ClosingValues:
                    query = "SELECT * FROM MBT_FECHAMENTO " + where;
                    break;
                case Table.Quotation:
                    query = "SELECT * FROM MERCADOBITCOIN " + where + " ORDER BY ID DESC LIMIT 1";
                    break;
                case Table.Orders:
                    query = "SELECT * FROM RBT_TRAD" + where;
                    break;
                case Table.Users:
                    query = "SELECT * FROM USR_CFIG" + where;
                    break;
            }

            try
            {
                if (OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    NpgsqlCommand cmd = new NpgsqlCommand(query, connection);
                    NpgsqlDataAdapter adp = new NpgsqlDataAdapter(cmd);
                    adp.Fill(data);

                    //close connection
                    CloseConnection();
                }
            }
            catch (NpgsqlException ex)
            {
                ReportError(ex);
            }
            finally
            {
                CloseConnection();
            }
            //open connection
            return data;
        }

        public static void Insert(Table tTable, Dictionary<string, string> Arguments)
        {
            try
            {
                if (OpenConnection() == true)
                {
                    string pFields = string.Empty;
                    string pValues = string.Empty;

                    foreach (KeyValuePair<string, string> args in Arguments)
                    {
                        pFields += args.Key + ",";
                        pValues += "'" + args.Value + "',";
                    }

                    string cmdInserir = @"Insert Into " + tTable.ToFriendlyString() + "" +
                        "(" + pFields.TrimEnd(',') + ") values" +
                        "(" + pValues.Trim(',') + ")";

                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdInserir, connection))
                    {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                ReportError(ex);
            }
            finally
            {
                CloseConnection();
            }
        }

        public static void Update(Table tTable, Dictionary<string, string> Arguments, string WhereId)
        {
            try
            {
                if (OpenConnection() == true)
                {
                    string pValues = string.Empty;

                    foreach (KeyValuePair<string, string> args in Arguments)
                        pValues += args.Key + "='" + args.Value + "',";

                    string cmdInserir = @"Update " + tTable.ToFriendlyString() + " SET " +
                        pValues.Trim(',') + " " + WhereId;

                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(cmdInserir, connection))
                    {
                        pgsqlcommand.ExecuteNonQuery();
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                ReportError(ex);
            }
            finally
            {
                CloseConnection();
            }
        }

        private static void ReportError(NpgsqlException ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(" ");
            Console.WriteLine(string.Format(UTIL.Constants.FATAL_ERROR_OCCURRED, ex.TargetSite.Name));
            Console.WriteLine(string.Format(UTIL.Constants.DETAILS, ex.Message.ToString()));
            Console.ResetColor();
        }
    }
}

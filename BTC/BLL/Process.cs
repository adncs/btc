﻿using BTC.DTO;
using System;
using System.Collections.Generic;
using System.Data;

namespace BTC.BLL
{
    /// <summary>
    /// Operações de processo (Robot => Data)
    /// </summary>
    public static class Process
    {
        public static void Buy(Coin coin, double PurchasePrice, bool IsEnabled)
        {
            try
            {
                Dictionary<string, string> dBuy = new Dictionary<string, string>();

                dBuy.Add("id_exg", ((int)Exchange.MercadoBitcoin).ToString());
                dBuy.Add("currency", coin.ToFriendlyString());
                dBuy.Add("value", PurchasePrice.ToString().Replace(",", "."));
                dBuy.Add("sense", "1");
                dBuy.Add("released", IsEnabled ? "1" : "0");
                dBuy.Add("dat_atlz", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                DAL.PgSQL.Insert(Table.Orders, dBuy);

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("[+] Ordem registrada!");
                Console.WriteLine(" ");
                Console.ResetColor();

            }
            catch (Exception ex)
            {
                Robot.Talk.ReportError(ex);
            }
        }

        public static void UpdateBuy(int Id, double? PurchasePrice, bool IsEnabled)
        {
            try
            {
                Dictionary<string, string> dBuy = new Dictionary<string, string>();

                if (PurchasePrice != null) dBuy.Add("value", PurchasePrice.ToString().Replace(",", "."));
                dBuy.Add("released", IsEnabled ? "1" : "0");
                dBuy.Add("dat_atlz", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                DAL.PgSQL.Update(Table.Orders, dBuy, "WHERE id_trad=" + Id);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("[+] Ordem aprovada!");
                Console.WriteLine(" ");
                Console.ResetColor();
            }
            catch (Exception ex)
            {
                Robot.Talk.ReportError(ex);
            }
        }

        public static void Sell(Coin coin, double SalePrice, bool IsEnabled)
        {
            try
            {
                Dictionary<string, string> dSell = new Dictionary<string, string>();

                dSell.Add("id_exg", ((int)Exchange.MercadoBitcoin).ToString());
                dSell.Add("currency", coin.ToFriendlyString());
                dSell.Add("sense", "2");
                dSell.Add("value", SalePrice.ToString().Replace(",", "."));
                dSell.Add("released", IsEnabled ? "1" : "0");
                dSell.Add("dat_atlz", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                DAL.PgSQL.Insert(Table.Orders, dSell);

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("[+] Ordem registrada!");
                Console.WriteLine(" ");
                Console.ResetColor();
            }
            catch (Exception ex)
            {
                Robot.Talk.ReportError(ex);
            }
        }

        public static void UpdateSell(int Id, double? SalePrice, bool IsEnabled)
        {
            try
            {
                Dictionary<string, string> dSell = new Dictionary<string, string>();

                if (SalePrice != null) dSell.Add("value", SalePrice.ToString().Replace(",", "."));
                dSell.Add("released", IsEnabled ? "1" : "0");
                dSell.Add("dat_atlz", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                DAL.PgSQL.Update(DTO.Table.Orders, dSell, "WHERE id_trad=" + Id);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("[+] Ordem aprovada!");
                Console.WriteLine(" ");
                Console.ResetColor();
            }
            catch (Exception ex)
            {
                Robot.Talk.ReportError(ex);
            }
        }

        public static List<Orders> Orders()
        {
            try
            {
                List<Orders> Order = new List<Orders>();
                DataTable dOrders = DAL.PgSQL.Select(Table.Orders, null);

                foreach (DataRow ord in dOrders.Rows)
                {
                    Order = new List<Orders>();
                    Order.Add(new Orders
                    {
                        Id = Convert.ToInt32(ord["Id_trad"]),
                        Exchange = (Exchange)Convert.ToInt32(ord["Id_exg"]),
                        Currency = ord["Currency"].ToString(),
                        Released = Convert.ToBoolean(ord["Released"]),
                        Sense = (Operation)ord["Sense"],
                        Value = Convert.ToDouble(ord["Value"]),
                        Date = Convert.ToDateTime(ord["Dat_atlz"])
                    });
                }

                return Order;
            }
            catch (Exception ex)
            {
                Robot.Talk.ReportError(ex);
                return new List<Orders>();
            }
        }

        public static double CurrentPrice(Coin coin)
        {
            try
            {
                //Retorna valor atual
                DataTable dQuotation = DAL.PgSQL.Select(Table.Quotation, "WHERE CURRENCY='" + coin.ToFriendlyString() + "'");

                return Convert.ToDouble(dQuotation.Rows[0].ItemArray[3]);
            }
            catch (Exception ex)
            {
                Robot.Talk.ReportError(ex);
                return 0;
            }
        }
    }
}

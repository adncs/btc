﻿using BTC.BLL;
using BTC.DTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace BTC.Robot
{
    public static class Talk
    {
        public static void Initialize()
        {
            ArrayList line = new ArrayList();

            line.Add("Iniciando Sistema");
            line.Add("---------------------------------");
            line.Add("HORA: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            line.Add("PAGE: http://mrladeia.com/currency/requests/debug/");
            line.Add("VERS: 0.1b");
            line.Add("---------------------------------");

            ReportExternal("Hi there!", line);
        }

        public static void ToClose()
        {
            ArrayList line = new ArrayList();

            line.Add("Sistema Encerrado");
            line.Add("---------------------------------");
            line.Add("HORA: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            line.Add("---------------------------------");

            ReportExternal("See you later!", line);
        }

        public static void This(string Text)
        {
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("[ROBOT-SAYS]: " + Text);
            Console.WriteLine(" ");
            Console.WriteLine(" ");
        }

        public static void TechnicalAnalysis(Coin coin, Operation pOperation, ArrayList Log, bool bExternal)
        {
            ArrayList line = new ArrayList();
            Dictionary<string, string> Arguments = new Dictionary<string, string>();

            string lastPrice = Process.CurrentPrice(coin).ToString();

            Arguments.Add("id_exg", "1");
            Arguments.Add("currency", coin.ToFriendlyString());
            Arguments.Add("log", string.Join(" \n ", Log.ToArray()));
            Arguments.Add("sense", pOperation.ToString());
            Arguments.Add("value", lastPrice.Replace(",", "."));
            Arguments.Add("dat_cria", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

            if (pOperation == Operation.Buy)
                Console.ForegroundColor = ConsoleColor.Green;
            else if (pOperation == Operation.Sell)
                Console.ForegroundColor = ConsoleColor.Red;
            else if (pOperation == Operation.None)
                Console.ResetColor();

            line.Add("--------------------------------------");
            line.Add(string.Format("Sincronizando dados de: {0}", coin.ToFullString()));
            line.Add(" ");
            line.Add(string.Format("SYNCR: {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            line.Add(string.Format("SENSE: {0}", pOperation.ToFriendlyString()));
            line.Add(string.Format("VALUE: {0}", lastPrice));
            line.Add(string.Format("REPRT: {0}", string.Join("\n       ", Log.ToArray())));
            line.Add("--------------------------------------");
            line.Add(" ");

            DAL.PgSQL.Insert(Table.Log, Arguments);

            if ((pOperation != Operation.None) && bExternal)
                ReportExternal(pOperation.ToFriendlyString() + " de " + coin.ToFullString(), line);

            ReportInternal(line);
        }

        private static void ReportInternal(ArrayList Log)
        {
            foreach (string line in Log)
                Console.WriteLine(line);
        }

        public static void ReportExternal(string Title, ArrayList Log)
        {
            try
            {
                WebRequest request = WebRequest.Create("https://api.pushbullet.com/v2/pushes");
                request.Method = "POST";
                request.Headers.Add("Access-Token", "o.QJoXxcvj7CPKxq3OyM87bS6UQVE9qfRp");
                request.ContentType = "application/json; charset=UTF-8";

                string postData =
                    "{\"body\":\"" + string.Join(" \\n ", Log.ToArray()) +
                    "\",\"title\":\"" + Title +
                    "\",\"type\":\"note\",\"channel_tag\":\"robotcoin\"}";

                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
            }
            catch (Exception ex)
            {
                ReportError(ex);
            }
        }

        public static void ReportError(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(" ");
            Console.WriteLine(string.Format(UTIL.Constants.FATAL_ERROR_OCCURRED, ex.TargetSite.Name));
            Console.WriteLine(string.Format(UTIL.Constants.DETAILS, ex.Message.ToString()));
            Console.ResetColor();
        }
    }
}

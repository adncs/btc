﻿using BTC.BLL;
using BTC.DTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTC.Robot
{
    public static class Study
    {
        private static bool Initialized = false;
        private static bool Report = false;
        private static bool InStudy = false;
        private static Dictionary<Coin, Sense> Reference = new Dictionary<Coin, Sense>();

        public static List<Statistics> Run()
        {
            if (!InStudy)
            {
                //Inicia estudo
                InStudy = !InStudy;

                List<Statistics> dStatistics = new List<Statistics>();

                Console.WriteLine(" ");
                Console.WriteLine(" ");
                Console.WriteLine("Iniciando estudos... [" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "]");

                //Se for initialize:
                if (!Initialized)
                {
                    Talk.Initialize();
                    Initialized = !Initialized;
                }
                if(Reference.Count == 0)
                {
                    Reference.Add(Coin.Bitcoin, Sense.Neuter);
                    Reference.Add(Coin.BitcoinCash, Sense.Neuter);
                    Reference.Add(Coin.Litecoin, Sense.Neuter);
                }

                dStatistics.Add(RSI(Coin.Bitcoin));
                dStatistics.Add(RSI(Coin.BitcoinCash));
                dStatistics.Add(RSI(Coin.Litecoin));

                //Se for meio dia/meia noite reporta status geral:
                if ((DateTime.Now.Hour == 12 || DateTime.Now.Hour == 0) && !Report)
                {
                    ArrayList line = new ArrayList();
                    line.Add("---------------------------------");

                    foreach (Statistics sts in dStatistics)
                        line.Add("[" + sts.Coin.ToFriendlyString() + "] RSI: " + sts.RSI);

                    line.Add("---------------------------------");

                    Talk.ReportExternal("Resumo Geral", line);
                    Report = !Report;
                }
                else if (DateTime.Now.Hour == 13 || DateTime.Now.Hour == 1)
                    Report = false;

                //Encerra estudo
                InStudy = !InStudy;
            }

            return new List<Statistics>();
        }

        private static Statistics RSI(Coin coin)
        {

            //TODO: Utilizar StochRSI

            //Busco o último ponto de RSI, e trabalho conforme resultado
            double _lastRsi = Math.Round(Statistic.RSI.LastRSI(coin), 3);

            if (_lastRsi == -1)
            {
                Talk.TechnicalAnalysis(coin, Operation.None, new ArrayList() { ("Not enough records") }, false);
                return null;
            }

            //Caso último ponto RSI inferior a 30, representação de sobrevendido (zona de compra)
            if (_lastRsi <= 30)
            {
                /*
                 * Zona de compra (SINAL VERDE) 
                 */

                ToBuy(coin, _lastRsi);
            }
            //Caso último ponto RSI superior a 70, representação de sobrecomprado (zona de venda)
            else if (_lastRsi > 70)
            {
                /*
                 * Zona de venda (SINAL VERMELHO) 
                 */

                ToSell(coin, _lastRsi);
            }
            //Configurações de alto risco deverão trabalhar em sub-estatísticas nesta seção
            else
            {
                /*
                 * Zona de segurança,
                 * aguarde o próximo sinal
                 */

                NoSignal(coin, _lastRsi);
            }

            return new Statistics { Coin = coin, RSI = _lastRsi };
        }

        private static void NoSignal(Coin coin, double _lastRsi)
        {
            Talk.TechnicalAnalysis(coin, Operation.None, new ArrayList() { "RSI: " + _lastRsi }, false);

            //TODO: Checar se não ficou ordem. Caso tenha, liberar
            if (Process.Orders().Exists(o => o.Sense == Operation.Sell && o.Currency == coin.ToFriendlyString() && !o.Released))
                Process.UpdateSell(Process.Orders().Find(o => o.Sense == Operation.Sell && o.Currency == coin.ToFriendlyString() && !o.Released).Id, null, true);
            if (Process.Orders().Exists(o => o.Sense == Operation.Buy && o.Currency == coin.ToFriendlyString() && !o.Released))
                Process.UpdateBuy(Process.Orders().Find(o => o.Sense == Operation.Buy && o.Currency == coin.ToFriendlyString() && !o.Released).Id, null, true);

            Reference[coin] = Sense.Neuter;
        }

        private static void ToSell(Coin coin, double _lastRsi)
        {
            Talk.TechnicalAnalysis(coin, Operation.Sell, new ArrayList() { "RSI: " + _lastRsi }, (Reference[coin] != Sense.High));

            if (Process.Orders().Exists(o => o.Sense == Operation.Sell && o.Currency == coin.ToFriendlyString() && !o.Released))
            {
                int mOrder = Process.Orders().Find(o => o.Sense == Operation.Sell && o.Currency == coin.ToFriendlyString() && !o.Released).Id;

                if (CurveAnalysis(Sense.High) == Sense.High)
                    Process.UpdateSell(mOrder, Process.CurrentPrice(coin) - 10, false);
                else
                    Process.UpdateSell(mOrder, null, true);
            }
            else
            {
                if (Reference[coin] != Sense.High)
                    Process.Sell(coin, Process.CurrentPrice(coin), false);
            }

            Reference[coin] = Sense.High;
        }

        private static void ToBuy(Coin coin, double _lastRsi)
        {
            Talk.TechnicalAnalysis(coin, Operation.Buy, new ArrayList() { "RSI: " + _lastRsi }, (Reference[coin] != Sense.Falls)); //Reporta análise RSI

            //NOTA: Em cada zona de compra/venda, será registrado em buffer 1 ordem. Independente se subir em varias frações
            //      O perfil de investidor do usuário define se ele terá saldo para entrar em todas GoldSessions repetidas sequencialmente

            /*
             * Verifica se já existe ordem nessa GoldSession aguardando liberação */
            if (Process.Orders().Exists(o => o.Sense == Operation.Buy && o.Currency == coin.ToFriendlyString() && !o.Released))
            {
                /* Se existir, pega ID da ordem, avalia tendencia, atualiza valor de compra ou libera trade */
                int mOrder = Process.Orders().Find(o => o.Sense == Operation.Buy && o.Currency == coin.ToFriendlyString() && !o.Released).Id;

                if (CurveAnalysis(Sense.Falls) == Sense.Falls) //Tendencia de mais queda
                    Process.UpdateBuy(mOrder, Process.CurrentPrice(coin) - 10, false); //Subtrai, e aguarda próxima iteração
                else //Tendencia de queda encerrada
                    Process.UpdateBuy(mOrder, null, true); //Libera ordem imediatamente
            }
            else
            {
                //   Se a última referencia for neutra
                if (Reference[coin] != Sense.Falls)
                    Process.Buy(coin, Process.CurrentPrice(coin), false);
            }

            Reference[coin] = Sense.Falls;
        }

        private static Sense CurveAnalysis(Sense ySense)
        {
            /*
             * Analisar lista de estatística para
             * avaliar nível da curva e indicar apontamento
             * de queda ou alta
             */

            //TODO: Não implementado

            if (ySense == Sense.High)
                return Sense.Falls;

            return Sense.High;
        }


    }
}

﻿using BTC.DTO;
using System;
using System.Data;
using System.Linq;
using TicTacTec.TA.Library;

namespace BTC.Statistic
{
    public static class RSI
    {
        public static double[] RSInalysis(Coin coin)
        {
            try
            {
                DataTable dData = DAL.PgSQL.Select(DTO.Table.ClosingValues, "WHERE CURRENCY='" + coin.ToFriendlyString() + "'");

                int ix = 0;
                double[] inReal = new double[dData.Rows.Count];
                var startIdx = 0;
                var endIdx = inReal.Length - 1;
                var period = 14;
                var outBegIdx = -1;
                var outNBElement = -1;
                var outReal = new double[endIdx];

                foreach (DataRow row in dData.Rows)
                    inReal[ix++] = Double.Parse(row["last"].ToString());

                var result = Core.Rsi(startIdx, endIdx, inReal, period, out outBegIdx, out outNBElement, outReal);

                return outReal.Where(x => x != 0).ToArray();
            }
            catch (Exception ex)
            {
                Robot.Talk.ReportError(ex);
                return new double[] { 0 };
            }
        }

        #region Support Analysis

        public static double LastRSI(Coin coin)
        {
            try
            {
                double[] RSI = RSInalysis(coin);

                if (RSI.Count() == 0)
                    return -1;

                return RSI[RSI.Length - 1];
            }
            catch (Exception ex)
            {
                Robot.Talk.ReportError(ex);
                return 0;
            }
        }

        #endregion
    }
}

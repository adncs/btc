﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BTC.API
{
    public static class MercadoBitcoin
    {
        public static void teste()
        {
            var request = (HttpWebRequest)WebRequest.Create("https://www.mercadobitcoin.net/tapi/v3/");

            var postData = "tapi_method=list_orders";
            postData += "&tapi_nonce=1";
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
    }
}

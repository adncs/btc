﻿using BTC.Statistic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TicTacTec.TA.Library;

namespace WFApp_Testes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            double[] data = RSI.RSInalysis(BTC.DTO.Coin.Bitcoin);

            //double[] data =
            //    {
            //        54.09,
            //        59.90,
            //        58.20,
            //        59.76,
            //        52.35,
            //        52.82,
            //        56.94,
            //        57.47,
            //        55.26,
            //        57.51,
            //        54.80,
            //        51.47,
            //        56.16,
            //        58.34,
            //        56.02,
            //        60.22,
            //        56.75,
            //        57.38,
            //        50.23,
            //        57.06,
            //        61.51,
            //        63.69,
            //        66.22,
            //        69.16,
            //        70.73,
            //        67.79,
            //        68.82,
            //        62.38,
            //        67.59,
            //        67.59
            //};

            double[] highest = new double[data.Length];
            double[] lowest = new double[data.Length];
            double high = 0;
            double low = 200;

            double[] stoch = new double[data.Length];

            for (int i = 13; i < data.Length; i++)
            {
                for (int j = i - 13; j <= i; j++)
                {
                    if (data[j] > high)
                        high = data[j];
                    if (data[j] <= low)
                        low = data[j];
                }

                highest[i] = high;
                lowest[i] = low;

                stoch[i] = Math.Round(((data[i] - lowest[i])) / (highest[i] - lowest[i]), 2);

                high = 0;
                low = 200;
            }

            //[RSI(14,price) - Minimum(14,RSI(14,price))] / [Maximum(14,RSI(14,price)) - Minimum(14,RSI(14,price))]


            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series[0].Points.DataBindY(stoch.Take(150).ToArray());

            //double[] inReal = data;
            //var startIdx = 0;
            //var endIdx = inReal.Length - 1;
            //var outBegIdx = -1;
            //var outNBElement = -1;
            //var outReal = new double[endIdx];
            //var outRealK = new double[endIdx];

            //var result = Core.StochRsi(startIdx, endIdx, inReal, 14, 14, 14, Core.MAType.Sma, out outBegIdx, out outNBElement, outReal, outRealK);

            //double[] b = new double[200];

            //Array.Copy(outRealK, b, 200);

            //\paragrafh{\hspace{1.75cm} }
            string A = @"\hspace{1.75cm";
            string.Format(@"\paragrafh{0}", A);
        }
    }
}

﻿using MySql.Data.MySqlClient;
using System.Data;
using System.Diagnostics;

namespace Adn.Mysql
{
    public class MySQL
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        //Initialize values
        private void Initialize()
        {
            server = "s.brasilturbo.com";
            database = "bitcoin";
            uid = "root";
            password = "o28oq78n";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                if(connection == null)
                    Initialize();

                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {

                switch (ex.Number)
                {
                    case 0:
                        Debug.Write("Não foi possível se conectar no banco de dados.  Contate o administrador");
                        break;

                    case 1045:
                        Debug.Write("Usuário e/ou senha inválido, por favor tente novamente");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Debug.Write(ex.Message);
                return false;
            }
        }

        //Insert statement

        public DataTable Select()
        {
            DataTable data = new DataTable();

            string query = "SELECT * FROM MBT_FECHAMENTO";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                adp.Fill(data);

                //close connection
                this.CloseConnection();

            }

            return data;
        }
    }
}
